var elixir = require('laravel-elixir');

// elixir(function(mix) {
//     mix.less('app.less');
//
// });

elixir(function(mix) {
    mix.less([
        'less.less',
        'less_2.less',
        'less_3.less',
    ], 'public/css/css.css');
});

elixir(function(mix) {
    mix.sass([
        'sass.scss'
    ], 'public/css/css_2.css');
});